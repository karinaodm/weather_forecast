"""
Load and clean data
"""
import sys
import re
import pandas as pd
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv


def load_data(file_path: str) -> pd.DataFrame:
    """Load data from file

    Args:
        file_path (str): path to data file

    Returns:
        pd.DataFrame: data frame
    """
    return pd.read_csv(file_path, header=7, sep=";")


def fix_csv_file(in_file_path: str, out_file_path):
    """Fixes problems in csv_file and save new correct one

    Args:
        in_file_path (str): input file path
        out_file_path (_type_): output file path
    """

    try:
        csv_file = open(in_file_path, "r")
    except FileNotFoundError:
        print(f"File {in_file_path} not found.  Aborting")
        sys.exit(1)

    out_csv_file = open(out_file_path, "w")

    with csv_file:
        for line in csv_file.readlines():
            if "span" in line:
                start = "<"
                end = ">"
                res = re.findall(rf"\{start}.*?\{end}", line)
                if len(res) == 4:
                    start = res[2]
                    end = res[3]
                    replaced_str = re.findall(rf"\{start}.*?\{end}", line)[0]
                    line = line.replace(replaced_str, "")
                    start = res[0]
                    end = res[1]
                    new_text = re.findall(rf"{start}(.*?){end}", line)[0]
                    replaced_str = re.findall(rf"\{start}.*?\{end}", line)[0]
                    line = line.replace(replaced_str, new_text)
            out_csv_file.write(line)
    out_csv_file.close()

    try:
        load_data(out_file_path)
    except ValueError:
        pass


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(input_filepath, output_filepath):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')
    fix_csv_file(input_filepath, output_filepath)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
