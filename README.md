## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)

## Weather forecast
Weather forecast for aeroport. Used data from https://rp5.ru/%D0%90%D1%80%D1%85%D0%B8%D0%B2_%D0%BF%D0%BE%D0%B3%D0%BE%D0%B4%D1%8B_%D0%B2_%D0%91%D0%BE%D0%B3%D0%B0%D1%88%D1%91%D0%B2%D0%BE,_%D0%B8%D0%BC._%D0%9D._%D0%98._%D0%9A%D0%B0%D0%BC%D0%BE%D0%B2%D0%B0_(%D0%B0%D1%8D%D1%80%D0%BE%D0%BF%D0%BE%D1%80%D1%82),_METAR

## Description
Train and test model for weather forecasting.
### Load data
* Open [https://rp5.ru](https://rp5.ru/%D0%90%D1%80%D1%85%D0%B8%D0%B2_%D0%BF%D0%BE%D0%B3%D0%BE%D0%B4%D1%8B_%D0%B2_%D0%91%D0%BE%D0%B3%D0%B0%D1%88%D1%91%D0%B2%D0%BE,_%D0%B8%D0%BC._%D0%9D._%D0%98._%D0%9A%D0%B0%D0%BC%D0%BE%D0%B2%D0%B0_(%D0%B0%D1%8D%D1%80%D0%BE%D0%BF%D0%BE%D1%80%D1%82),_METAR)
* Choose CSV format and UTF-8
* Press Download. If it does not work then use `wget`


## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
* Clean dataset `python3 src/data/make_dataset.py data/raw/UNTT.21.11.2013.17.04.2022.1.0.0.ru.utf8.00000000.csv data/processed/data.csv`

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Development
* Create venv
```
python3 -m venv ./venv
source ./venv/bin/activate
pip3 install -r requirements.txt

```
* use auto formatting `black path/to/src`
